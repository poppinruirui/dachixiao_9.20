﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Polygon : MapObj {

    public List<PolygonPoint> m_lstPoint = new List<PolygonPoint>();
    Dictionary<string, PolygonLine> m_dicLine = new Dictionary<string, PolygonLine>();
    Dictionary<string, PolygonLine> m_dicLineTemp = new Dictionary<string, PolygonLine>();

    public EdgeCollider2D _collierEdge;
    public PolygonCollider2D _collierPolygon;

    public CircleCollider2D _colliderSeed;

    public string m_szFileName = "";

    //// !---- 草丛相关
    bool m_bIsGrass = false;
    bool m_bIsGrassSeed = false;
    bool m_bNowSeeding = false;
    Mesh m_meshGrass = null;
    public MeshRenderer m_mrGrass = null;
    Material m_matMain = null;

    public GameObject m_goSeed;
    Vector3 vecTempPos = new Vector3();
    Vector3 vecTempScale = new Vector3();

    int m_nOwnerPhotonId = -1;
    double m_dGrassActivatedTime = 0.0f;
    int m_nGUID = 0;

	int m_nHardObstacle = 0;

    static Vector2 Vec2Temp;

    // Use this for initialization
    void Start () {
        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game )
        {
           // _collierPolygon.enabled = false;
        }

    }
	
	// Update is called once per frame
	void Update () {
        AutoRecoverAlpha();
        SeedGrassCounting();
    }

    void Awake()
    {
        m_eObjType = eMapObjType.polygon;
        m_meshGrass = this.gameObject.GetComponent<MeshFilter>().mesh;
        m_mrGrass = this.gameObject.GetComponent<MeshRenderer>();
        m_matMain = m_mrGrass.materials[0];         

        Reset();
        if (AccountManager.m_eSceneMode  != AccountManager.eSceneMode.Game)
        {
            _collierEdge.isTrigger = true;
        }
        else
        {
            _collierEdge.isTrigger =false;
        }

		m_fSeedSize = m_goSeed.transform.localScale.x;
    }

    public void Reset()
    {
       
        for ( int i = m_lstPoint.Count - 1; i >= 0; i-- )
        {
            PolygonPoint point = m_lstPoint[i];
            point.UnRelatedToLine();
            GameObject.Destroy( point.gameObject );
        }

        m_lstPoint.Clear();
        m_dicLine.Clear();
    }

    public override void OnMouseDown()
    {
		if (MapEditor.s_Instance && AccountManager.m_eSceneMode == AccountManager.eSceneMode.MapEditor)
        {
            MapEditor.s_Instance.PickObj(this);
        }
    }

    public void OnPointListChange()
    {
        for (int i = 0; i < m_lstPoint.Count; i++)
        {
            PolygonPoint point = m_lstPoint[i];
            point.SetIndex(i);
        }
    }

    public void Link2PointsToGenerateLine(PolygonPoint p1, PolygonPoint p2)
    {
        PolygonLine line = PolygonEditor.NewLine();
        line.transform.parent = this.transform;
        string key = line.Link(p1, p2);
        AddOneLine(key, line);
        line.name = "line_" + key;
    }

    public void AddOneLine(string key, PolygonLine line)
    {
        m_dicLine[key] = line;
    }

    public void RemoveOneLine(string key)
    {
        PolygonLine line = m_dicLine[key];
        GameObject.Destroy( line.gameObject );
        m_dicLine.Remove(key);
    }

    public void RemovePoint(PolygonPoint point)
    {
        m_lstPoint.Remove(point);
        OnPointListChange();
        GameObject.Destroy(point.gameObject);
    }

    public void GeneratePolygon(string[] aryPointPos, bool bPolygonEditor = false)
    {
		if (aryPointPos.Length == 0) {
			return;
		}

        string[] aryTemp = null;
        Vector2[] collide_points = new Vector2[aryPointPos.Length + 1];
        for (int i = 0; i < aryPointPos.Length; i++)
        {
            PolygonPoint point = PolygonEditor.NewPoint();

            point.SetIndex(i);

            aryTemp = aryPointPos[i].Split(',');
            float fX = float.Parse(aryTemp[0]);
            float fY = float.Parse(aryTemp[1]);

            collide_points[i].x = fX;
            collide_points[i].y = fY;

            point.transform.localPosition = new Vector3(fX, fY, 0.0f);
            point.transform.parent = this.transform;
            m_lstPoint.Add(point);

            int nPrevIndex = 0;
            if (i > 0)
            {
                nPrevIndex = i - 1;
                PolygonPoint prev_point = m_lstPoint[nPrevIndex];
                Link2PointsToGenerateLine(point, prev_point);
            }

            /*
            if (i == aryPointPos.Length - 1)
            {
                nPrevIndex = 0;
                PolygonPoint prev_point = m_lstPoint[nPrevIndex];
                Link2PointsToGenerateLine(point, prev_point);
            }
            */
        } // end for

        // 生成碰撞器
		collide_points[collide_points.Length-1] = collide_points[0];
        _collierEdge.points = collide_points;
        _collierPolygon.points = collide_points;


        CalculateCenter();


        if (!bPolygonEditor)
        {
            for (int i = 0; i < m_lstPoint.Count; i++)
            {
                PolygonPoint point = m_lstPoint[i];
                point.gameObject.SetActive(false);
            }
        }
    }

    Vector2 m_vecCenter = new Vector2();
    void CalculateCenter()
    {
        m_vecCenter = Vector2.zero;
        for ( int i = 0; i < _collierEdge.points.Length; i++ )
        {
            m_vecCenter.x += _collierEdge.points[i].x;
            m_vecCenter.y += _collierEdge.points[i].y;
        }
        m_vecCenter.x = m_vecCenter.x / _collierEdge.points.Length;
        m_vecCenter.y = m_vecCenter.y / _collierEdge.points.Length;

        vecTempPos.x = m_vecCenter.x;
        vecTempPos.y = m_vecCenter.y;
        vecTempPos.z = -1.0f;
        //m_goSeed.transform.localPosition = vecTempPos;
    }

    public Vector2 GetCenter()
    {
        return m_vecCenter;
    }

    public bool GetIsGrass()
    {
        return m_bIsGrass;
    }

    public void SetIsGrassSeed(bool bIsGrassSeed)
    {
        m_bIsGrassSeed = bIsGrassSeed;
        if (m_bIsGrassSeed) // 是种子
        {
            if ( AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game )
            {
                GrassBecomeSeed();
            }
            m_goSeed.SetActive( true );
        }
        else // 不是种子
        {
            m_goSeed.SetActive(false);
        }
    }




    public bool GetIsGrassSeed()
    {
        return m_bIsGrassSeed;
    }

    public void SetIsGrass( bool bIsGrass )
    {
        if ( bIsGrass )
        {
            if ( !CheckIfCanGenerateGrass())
            {
                return;
            }
        }

        m_bIsGrass = bIsGrass;

        if (m_bIsGrass) // 是草坪
        {
            GenerateGrass();
            SetBorderLinesVisible(false);
			_collierEdge.enabled = false;
			_collierPolygon.isTrigger = true;
			this.transform.parent = MapEditor.s_Instance.m_goGrassContainer.transform;
        }
        else // 不是草坪
        {
            SetIsGrassSeed( false ); // 不是草坪自然也就谈不上“种子模式”
            ClearGrass();
            SetBorderLinesVisible(true);
            _collierEdge.isTrigger = false;
			this.transform.parent = MapEditor.s_Instance.m_goPolygonContainer.transform;
        }
    }

    public bool IsNowSeeding()
    {
        return m_bNowSeeding;
    }

    public void SeedBecomeGrass( int nPlayerPhotonId, double dCurTime)
    {
        if ( Main.s_Instance.GetMainPlayerPhotonId() == nPlayerPhotonId )
        {
            SetAlpha(0.4f);
        }
        else
        {
            SetAlpha(1.0f);
        }
        m_bNowSeeding = false;
		_collierEdge.enabled = false;
        _collierPolygon.enabled = true;
		_collierPolygon.isTrigger = true;
        _colliderSeed.enabled = false;
        m_goSeed.SetActive(false);
        m_nOwnerPhotonId = nPlayerPhotonId;
        m_dGrassActivatedTime = dCurTime;
    }

    public void GrassBecomeSeed()
    {
        SetAlpha(0.0f);
        _collierEdge.enabled = false;
        _collierPolygon.enabled = false;
        _colliderSeed.enabled = true;
        m_goSeed.SetActive(true);
        m_nOwnerPhotonId = -1;
        m_bNowSeeding = true;
    }

    public void SeedGrassCounting()
    {
		if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game) {
			return;
		}

        if ( !m_bIsGrass )
        {
            return;
        }

        if ( !m_bIsGrassSeed )
        {
            return;
        }

        if ( m_bNowSeeding )
        {
            return;
        }

        if (Main.GetTime() - m_dGrassActivatedTime >= GetSeedGrassLifeTime())
        {
            GrassBecomeSeed();
        }
    }

	float m_fSeedGrassLifeTime = 20.0f;
	public float GetSeedGrassLifeTime()
	{
		return m_fSeedGrassLifeTime;
	}

	public void SetSeedGrassLifeTime( float val )
	{
		m_fSeedGrassLifeTime = val;
	}

    void SetBorderLinesVisible( bool bVisible )
    {
        foreach( KeyValuePair<string, PolygonLine> pair in m_dicLine )
        {
            PolygonLine line = pair.Value;
            line.gameObject.SetActive(bVisible);
        }
    }

    bool CheckIfCanGenerateGrass()
    {
        if (m_lstPoint.Count < 3)
        {
            Debug.Log("顶点数小于3的多边形不能作草坪！！");
            return false; 
        }

        return true;
    }

    public void ClearGrass()
    {
        m_meshGrass.Clear();
    }

    public void GenerateGrass()//入口参数：存放顶点信息的vector3数组
    {
        Vector3[] vertices = new Vector3[m_lstPoint.Count];
        for ( int i = 0; i < m_lstPoint.Count; i++ )
        {
            vertices[i] = m_lstPoint[i].GetLocalPosition();
        }

        if (vertices == null)
        {
            return;
        }

        if (vertices.Length < 3)
        {
            return;
        }
        int numberOfTriangles = vertices.Length - 2;//三角形的数量等于顶点数减2

        int[] triangles = new int[numberOfTriangles * 3];//triangles数组大小等于三角形数量乘3
        int f = 0, b = vertices.Length - 1;//f记录前半部分遍历位置，b记录后半部分遍历位置
        for (int i = 1; i <= numberOfTriangles; i++)//每次给 triangles数组中的三个元素赋值，共赋值
        { //numberOfTriangles次
            if (i % 2 == 1)
            {
                triangles[3 * i - 3] = f++;
                triangles[3 * i - 2] = f;
                triangles[3 * i - 1] = b;//正向赋值，对于i=1赋值为：0,1,2
            }
            else
            {
                triangles[3 * i - 1] = b--;
                triangles[3 * i - 2] = b;
                triangles[3 * i - 3] = f;//逆向赋值，对于i=2赋值为：1,5,6
            }
        }
        m_meshGrass.vertices = vertices;
        m_meshGrass.triangles = triangles;//最后给mesh这个属性的vertices和triangles数组赋 值就ok~
		SetColor ( Color.green );
    }

	public void ClearHardObstacleMesh()
	{
		m_meshGrass.Clear();
	}

	public void GenerateHardObstacleMesh()//入口参数：存放顶点信息的vector3数组
	{
		Vector3[] vertices = new Vector3[m_lstPoint.Count];
		for ( int i = 0; i < m_lstPoint.Count; i++ )
		{
			vertices[i] = m_lstPoint[i].GetLocalPosition();
		}

		if (vertices == null)
		{
			return;
		}

		if (vertices.Length < 3)
		{
			return;
		}
		int numberOfTriangles = vertices.Length - 2;//三角形的数量等于顶点数减2

		int[] triangles = new int[numberOfTriangles * 3];//triangles数组大小等于三角形数量乘3
		int f = 0, b = vertices.Length - 1;//f记录前半部分遍历位置，b记录后半部分遍历位置
		for (int i = 1; i <= numberOfTriangles; i++)//每次给 triangles数组中的三个元素赋值，共赋值
		{ //numberOfTriangles次
			if (i % 2 == 1)
			{
				triangles[3 * i - 3] = f++;
				triangles[3 * i - 2] = f;
				triangles[3 * i - 1] = b;//正向赋值，对于i=1赋值为：0,1,2
			}
			else
			{
				triangles[3 * i - 1] = b--;
				triangles[3 * i - 2] = b;
				triangles[3 * i - 3] = f;//逆向赋值，对于i=2赋值为：1,5,6
			}
		}
		m_meshGrass.vertices = vertices;
		m_meshGrass.triangles = triangles;//最后给mesh这个属性的vertices和triangles数组赋 值就ok~

		SetColor ( Color.blue );
	}




    float m_fCurAlpha = 1.0f;
    public void SetAlpha( float fAlpha )
    {
        m_fCurAlpha = fAlpha;
        Color colorTemp = m_matMain.color;
        colorTemp.a = fAlpha;
        m_matMain.color = colorTemp;
    }

	public void SetColor( Color color )
	{
		m_matMain.color = color;
	}

    int m_nStatus = 0;
    public void SetStatus( int nStatus )
    {
        if (m_nStatus == nStatus)
        {
            return;
        }

        m_nStatus = nStatus;

        if (m_nStatus == 1)
        {
			m_fAlphaCounting = 1.0f;
            SetAlpha( 0.4f );
        }
    }

	float m_fAlphaCounting = 1.0f;
    void AutoRecoverAlpha()
    {
		if (m_fAlphaCounting > 0f) {
			m_fAlphaCounting -= Time.fixedDeltaTime;
			if (m_fAlphaCounting <= 0f) {
				SetStatus (0);
			}
		}

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
        {
            return;
        }

		/*
        if ( m_bIsGrassSeed && m_nOwnerPhotonId == Main.s_Instance.GetMainPlayerPhotonId() )
        {
            return;
        }
		*/

        if ( m_bNowSeeding )
        {
            return;
        }

        if (m_nStatus != 0)
        {
            return;
        }

        if (m_fCurAlpha >= 1.0f)
        {
            return;
        }
        m_fCurAlpha += 0.4f * Time.fixedDeltaTime;
        if (m_fCurAlpha >= 1.0f)
        {
            m_fCurAlpha = 1.0f;
        }

        SetAlpha(m_fCurAlpha);
    }

    public void SetOwnerPhotonId( int nId )
    {
        m_nOwnerPhotonId = nId;
    }

    public int GetOwnerPhotonId()
    {
        return m_nOwnerPhotonId;
    }

	float m_fSeedSize = 3f;
    public void SetScale( Vector3 scale )
    {
        this.transform.localScale = scale;

		vecTempScale.x = m_fSeedSize / scale.x;
		vecTempScale.y = m_fSeedSize / scale.y;
        vecTempScale.z = 1.0f;
        m_goSeed.transform.localScale = vecTempScale;
    }

    public void SetGUID( int nGUID )
    {
        m_nGUID = nGUID;
    }

    public int GetGUID()
    {
        return m_nGUID;
    }

	public bool IsHardObstacle()
	{
		return m_nHardObstacle == 1;
	}

	public void SetHardObstacle( int val )
	{
		m_nHardObstacle = val;
		if (val == 1) {
			GenerateHardObstacleMesh ();
			_collierPolygon.enabled = true;
			_collierEdge.enabled = false;
			this.gameObject.tag = "scene_obstacle";
		} else {
			ClearHardObstacleMesh ();
			if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.MapEditor) 
			{
				_collierPolygon.enabled = false;
			}
			_collierEdge.enabled = true;
		}
	}
}
