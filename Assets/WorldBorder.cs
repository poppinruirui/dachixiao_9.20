﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBorder : MonoBehaviour {

	public BoxCollider2D _colliderEdge;
    public LineRenderer _lr;

    Vector3 vecTempScale = new Vector3();
    Vector3 vecTempPos = new Vector3();
    Vector2 vecTemp2 = new Vector2();
    Vector2[] m_aryColliderPoints = new Vector2[2];


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

   
    public void SetSize(float width, float height, int type)
    {
        /*
        if (_colliderEdge == null)
        {
			_colliderEdge = this.gameObject.AddComponent<BoxCollider2D>();
        }
        */

        switch (type)
        {
            case 0: // top
                {
                    vecTempPos.x = -width / 2.0f;
                    vecTempPos.y = height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(0, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[0] = vecTemp2;
                    vecTempPos.x = width / 2.0f;
                    vecTempPos.y = height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(1, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[1] = vecTemp2;

                    vecTempScale.x = 3 * width;
                    vecTempScale.y = height;
                    vecTempScale.z = 1f;
                    CCHijiManager.s_Instance._goDeadMask[0].transform.localScale = vecTempScale;

                    vecTempPos.x = 0;
                    vecTempPos.y = height;
                    vecTempPos.z = 3f;
                    CCHijiManager.s_Instance._goDeadMask[0].transform.localPosition = vecTempPos;


                    /* “吃鸡版”不要边界阻挡
				_colliderEdge.size = new Vector3( width, 1f, 1f )  ;
				_colliderEdge.offset = new Vector3( 0f, height / 2 + 50f, 0f );
                    _colliderEdge.edgeRadius = 50;
           
                    */
                }
                break;
            case 1: // bottom
                {
                    vecTempPos.x = -width / 2.0f;
                    vecTempPos.y = -height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(0, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[0] = vecTemp2;

                    vecTempPos.x = width / 2.0f;
                    vecTempPos.y = -height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(1, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[1] = vecTemp2;
                    //    _colliderEdge.points = m_aryColliderPoints;


                    vecTempScale.x = 3 * width;
                    vecTempScale.y = height;
                    vecTempScale.z = 1f;
                    CCHijiManager.s_Instance._goDeadMask[1].transform.localScale = vecTempScale;

                    vecTempPos.x = 0;
                    vecTempPos.y =-height;
                    vecTempPos.z = 3f;
                    CCHijiManager.s_Instance._goDeadMask[1].transform.localPosition = vecTempPos;




                    /* “吃鸡版”不要边界阻挡
				_colliderEdge.size = new Vector3( width, 1f, 1f )  ;
				_colliderEdge.offset = new Vector3( 0f, -height / 2 - 50f, 0f );
                    _colliderEdge.edgeRadius = 50;
                    */
                }
                break;
            case 2: // left
                {
                    vecTempPos.x = -width / 2.0f;
                    vecTempPos.y = height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(0, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[0] = vecTemp2;

                    vecTempPos.x = -width / 2.0f;
                    vecTempPos.y = -height / 2.0f;
                    vecTempPos.z = 1.0f;
                    _lr.SetPosition(1, vecTempPos);
                    vecTemp2.x = vecTempPos.x;
                    vecTemp2.y = vecTempPos.y;
                    m_aryColliderPoints[1] = vecTemp2;
                    // _colliderEdge.points = m_aryColliderPoints;


                    vecTempScale.x = width;
                    vecTempScale.y = height;
                    vecTempScale.z = 1f;
                    CCHijiManager.s_Instance._goDeadMask[2].transform.localScale = vecTempScale;

                    vecTempPos.x = -width;
                    vecTempPos.y = 0;
                    vecTempPos.z = 3f;
                    CCHijiManager.s_Instance._goDeadMask[2].transform.localPosition = vecTempPos;

                    /* “吃鸡版”不要边界阻挡
                    _colliderEdge.size = new Vector3( 1f, height, 1f )  ;
                    _colliderEdge.offset = new Vector3( -width / 2f -  50f , 0f , 0f );
                    _colliderEdge.edgeRadius = 50;
                    */
                }
                break;
            case 3: // right
            {
            vecTempPos.x = width / 2.0f;
            vecTempPos.y = height / 2.0f;
            vecTempPos.z = 1.0f;
            _lr.SetPosition(0, vecTempPos);
            vecTemp2.x = vecTempPos.x;
            vecTemp2.y = vecTempPos.y;
            m_aryColliderPoints[0] = vecTemp2;

            vecTempPos.x = width / 2.0f;
            vecTempPos.y = -height / 2.0f;
            vecTempPos.z = 1.0f;
            _lr.SetPosition(1, vecTempPos);
            vecTemp2.x = vecTempPos.x;
            vecTemp2.y = vecTempPos.y;
            m_aryColliderPoints[1] = vecTemp2;


                    vecTempScale.x = width;
                    vecTempScale.y = height;
                    vecTempScale.z = 1f;
                    CCHijiManager.s_Instance._goDeadMask[3].transform.localScale = vecTempScale;

                    vecTempPos.x = width;
                    vecTempPos.y = 0;
                    vecTempPos.z = 3f;
                    CCHijiManager.s_Instance._goDeadMask[3].transform.localPosition = vecTempPos;


                    //   _colliderEdge.points = m_aryColliderPoints;

                    /* “吃鸡版”不要边界阻挡
                    _colliderEdge.size = new Vector3( 1f, height, 1f )  ;
            _colliderEdge.offset = new Vector3( width / 2f + 50f , 0f , 0f );
            _colliderEdge.edgeRadius = 50;

                    */
                } // end case 3
                break;
            } // end switch


        //        _colliderEdge.enabled = true;

        for (int i = 0; i < CCHijiManager.s_Instance._goDeadMask.Length; i++ )
        {
            CCHijiManager.s_Instance._goDeadMask[i].gameObject.SetActive( true );
        }

        } // end func
}
