﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class CBlockDivide : MonoBehaviour {

	public static CBlockDivide s_Instance = null;

	static Vector3 vecTempPos = new Vector3 ();
	static Vector3 vecTempScale = new Vector3 ();
	static Vector2 vecTempDir = new Vector2();

	public Color[] m_aryLineColor;
	public Color[] m_aryBallColor;

	public GameObject m_preLine;
	public GameObject m_preBlock;


	public float m_fWorldSize = 1000;
	public int m_nHierarchyNum = 0;

	float[] m_aryBlockScaleOfEveryHierarchy;
    uint[] m_aryHierachyReverseKey;
    uint[] m_aryHierachyKey; 
    

	// containers
	public GameObject m_goArea0;
	public GameObject m_goArea0_Stains;

	int m_nBlockNum = 0;

	public const uint INVALID_PARENT_KEY = 0;
	public const int HIGHEST_HIERARCHY_NO = 0;
	public GameObject _ball;


	public enum eBlockPosType
	{
		none = 0,
		left_up,
		right_up,
		left_down, 
		right_down,
	};

	void Awake()
	{
		s_Instance = this;
	}

	public Color GetColorByPlayerId( int nPlayerId )
	{
		return m_aryBallColor[nPlayerId];
	}

	public uint GetHierarchyKeybase( int nHierarchyNo )
	{
		return m_aryHierachyKey[nHierarchyNo];
	}

    public uint GetHierarchyReverseKey(int nHierarchyNo)
    {
        return m_aryHierachyReverseKey[nHierarchyNo];
    }

    // Use this for initialization
    void Start () {
	//	DrawWorldBorder ();
		//DrawDivideLines ( 0, 0, 0, m_fWorldSize );

		/*
		CBlock block = GameObject.Instantiate (m_preBlock).GetComponent<CBlock> ();
		float fScale = 1000f;
		vecTempScale.x = fScale;
		vecTempScale.y = fScale;
		vecTempScale.z = 1f;
		block.transform.localScale = vecTempScale;
		*/
		m_aryBlockScaleOfEveryHierarchy = new float[m_nHierarchyNum];
		m_aryHierachyKey = new uint[m_nHierarchyNum];
        m_aryHierachyReverseKey = new uint[m_nHierarchyNum];

        for (int i = 0; i < m_nHierarchyNum; i++) {
			m_aryBlockScaleOfEveryHierarchy [i] = m_fWorldSize / Mathf.Pow ( 2, i + 1 );
			m_aryHierachyKey [i] = (uint)Mathf.Pow ( 10, m_nHierarchyNum - i - 1 );
            m_aryHierachyReverseKey[i] = (uint)Mathf.Pow(10, m_nHierarchyNum - i);

        }

		// 生成最高层级的四个区块(“0”级区块是层级最高的区块)
		uint uKey = (uint)eBlockPosType.left_up * GetHierarchyKeybase( 0 );
		CBlock block = NewBlock(uKey);
		float fScale = m_aryBlockScaleOfEveryHierarchy[0];
		float fHalfScale = fScale / 2f;
		vecTempScale.x = fScale;
		vecTempScale.y = fScale;
		vecTempScale.z = 1f;
		block.SetScale ( vecTempScale );
		vecTempPos.x = -fHalfScale;
		vecTempPos.y = fHalfScale;
		vecTempPos.z = 0f;
		block.SetPos ( vecTempPos );
		block.SetPosType ( eBlockPosType.left_up );
		block.SetHierarchy ( 0 );
	
		block.SetKey (uKey);
		block.SetParentKey ( INVALID_PARENT_KEY );


		uKey = (uint)eBlockPosType.right_up * GetHierarchyKeybase( 0 );
		block = NewBlock(uKey);
		vecTempScale.x = fScale;
		vecTempScale.y = fScale;
		vecTempScale.z = 1f;
		block.SetScale ( vecTempScale );
		vecTempPos.x = fHalfScale;
		vecTempPos.y = fHalfScale;
		vecTempPos.z = 0f;
		block.SetPos ( vecTempPos );
		block.SetHierarchy ( 0 );

		block.SetPosType ( eBlockPosType.right_up );
		block.SetKey (uKey);
		block.SetParentKey ( INVALID_PARENT_KEY );

		// --
		uKey = (uint)eBlockPosType.left_down * GetHierarchyKeybase( 0 );
		block = NewBlock(uKey);
		vecTempScale.x = fScale;
		vecTempScale.y = fScale;
		vecTempScale.z = 1f;
		block.SetScale ( vecTempScale );
		vecTempPos.x = -fHalfScale;
		vecTempPos.y = -fHalfScale;
		vecTempPos.z = 0f;
		block.SetPos ( vecTempPos );
		block.SetHierarchy ( 0 );
		

		block.SetPosType ( eBlockPosType.left_down );
		block.SetKey (uKey);
		block.SetParentKey ( INVALID_PARENT_KEY );
		// ---
		uKey = (uint)eBlockPosType.right_down * GetHierarchyKeybase( 0 );
		block = NewBlock(uKey);
		vecTempScale.x = fScale;
		vecTempScale.y = fScale;
		vecTempScale.z = 1f;
		block.SetScale ( vecTempScale );
		vecTempPos.x = fHalfScale;
		vecTempPos.y = -fHalfScale;
		vecTempPos.z = 0f;
		block.SetPos ( vecTempPos );
		block.SetHierarchy ( 0 );
		

		block.SetPosType ( eBlockPosType.right_down );
		block.SetKey (uKey);
		block.SetParentKey ( INVALID_PARENT_KEY );

		// end 生成最高层级的四个区块
	}

	public int GetMaxHierarchyNo()
	{
		return m_nHierarchyNum - 1;
	}

	public float GetBlockScaleByHierarchyNo( int nHierarchyNo )
	{
		return m_aryBlockScaleOfEveryHierarchy[nHierarchyNo];
	}

	// Update is called once per frame
	float m_fTimeElapse = 0;
	void Update () {
		m_fTimeElapse += Time.deltaTime;
		if (m_fTimeElapse >= 1) {
			//Debug.Log ( m_goArea0.transform.childCount );
		}
	

		if (Input.GetMouseButtonDown (0)) {
			m_bMoving = !m_bMoving;


		}

	}

	bool m_bMoving = false;


	void DrawDivideLines( int nCurHierachy, float fBlockCenterX, float fBlockCenterY, float fBlockSize )
	{
		float fHalfBlockSize = fBlockSize / 2f;

		LineRenderer lrX = GameObject.Instantiate (m_preLine).GetComponent<LineRenderer> ();
		vecTempPos.x = fBlockCenterX -fHalfBlockSize;
		vecTempPos.y = fBlockCenterY;
		vecTempPos.z = 0;
		lrX.SetPosition (0, vecTempPos);
		vecTempPos.x = fBlockCenterX + fHalfBlockSize;
		vecTempPos.y = fBlockCenterY;
		vecTempPos.z = 0;
		lrX.SetPosition (1, vecTempPos);
		lrX.startColor = Color.red;;
		lrX.endColor = Color.red;;

		LineRenderer lrY = GameObject.Instantiate (m_preLine).GetComponent<LineRenderer> ();
		vecTempPos.x = fBlockCenterX;
		vecTempPos.y = fBlockCenterY + fHalfBlockSize;
		vecTempPos.z = 0;
		lrY.SetPosition (0, vecTempPos);
		vecTempPos.x = fBlockCenterX;
		vecTempPos.y = fBlockCenterY-fHalfBlockSize;
		vecTempPos.z = 0;
		lrY.SetPosition (1, vecTempPos);
		lrY.startColor = Color.red;
		lrY.endColor = Color.red;

		float fWidth = 0.2f;
		lrX.startWidth = fWidth;
		lrX.endWidth = fWidth;
		lrY.startWidth = fWidth;
		lrY.endWidth = fWidth;

		if (nCurHierachy == m_nHierarchyNum - 1) {
			m_nBlockNum += 4;
	
		}

		if (nCurHierachy >= m_nHierarchyNum - 1) {
			return;
		}

		float fQuarterBlockSize = fHalfBlockSize / 2f;

		float fChildCenterX = fBlockCenterX - fQuarterBlockSize;
		float fChildCenterY = fBlockCenterY + fQuarterBlockSize;
		DrawDivideLines ( nCurHierachy + 1, fChildCenterX, fChildCenterY, fHalfBlockSize );

	    fChildCenterX = fBlockCenterX + fQuarterBlockSize;
	    fChildCenterY = fBlockCenterY + fQuarterBlockSize;
		DrawDivideLines ( nCurHierachy + 1, fChildCenterX, fChildCenterY, fHalfBlockSize );

		fChildCenterX = fBlockCenterX - fQuarterBlockSize;
		fChildCenterY = fBlockCenterY - fQuarterBlockSize;
		DrawDivideLines ( nCurHierachy + 1, fChildCenterX, fChildCenterY, fHalfBlockSize );

		fChildCenterX = fBlockCenterX + fQuarterBlockSize;
		fChildCenterY = fBlockCenterY - fQuarterBlockSize;
		DrawDivideLines ( nCurHierachy + 1, fChildCenterX, fChildCenterY, fHalfBlockSize );
	}

	void DrawWorldBorder()
	{
		float fHalfBlockSize = m_fWorldSize / 2f;

		LineRenderer lrWorldLeft = GameObject.Instantiate (m_preLine).GetComponent<LineRenderer> ();
		vecTempPos.x = fHalfBlockSize;
		vecTempPos.y = fHalfBlockSize;
		vecTempPos.z = 0;
		lrWorldLeft.SetPosition (0, vecTempPos);
		vecTempPos.x = fHalfBlockSize;
		vecTempPos.y = -fHalfBlockSize;
		vecTempPos.z = 0;
		lrWorldLeft.SetPosition (1, vecTempPos);
		lrWorldLeft.startColor = m_aryLineColor [0];
		lrWorldLeft.endColor = m_aryLineColor [0];

		LineRenderer lrWorldRight = GameObject.Instantiate (m_preLine).GetComponent<LineRenderer> ();
		vecTempPos.x = -fHalfBlockSize;
		vecTempPos.y = fHalfBlockSize;
		vecTempPos.z = 0;
		lrWorldRight.SetPosition (0, vecTempPos);
		vecTempPos.x = -fHalfBlockSize;
		vecTempPos.y = -fHalfBlockSize;
		vecTempPos.z = 0;
		lrWorldRight.SetPosition (1, vecTempPos);
		lrWorldRight.startColor = m_aryLineColor [0];
		lrWorldRight.endColor = m_aryLineColor [0];

		LineRenderer lrWorldTop = GameObject.Instantiate (m_preLine).GetComponent<LineRenderer> ();
		vecTempPos.x = -fHalfBlockSize;
		vecTempPos.y = fHalfBlockSize;
		vecTempPos.z = 0;
		lrWorldTop.SetPosition (0, vecTempPos);
		vecTempPos.x = fHalfBlockSize;
		vecTempPos.y = fHalfBlockSize;
		vecTempPos.z = 0;
		lrWorldTop.SetPosition (1, vecTempPos);
		lrWorldTop.startColor = m_aryLineColor [0];
		lrWorldTop.endColor = m_aryLineColor [0];

		LineRenderer lrWorldBottom = GameObject.Instantiate (m_preLine).GetComponent<LineRenderer> ();
		vecTempPos.x = -fHalfBlockSize;
		vecTempPos.y = -fHalfBlockSize;
		vecTempPos.z = 0;
		lrWorldBottom.SetPosition (0, vecTempPos);
		vecTempPos.x = fHalfBlockSize;
		vecTempPos.y = -fHalfBlockSize;
		vecTempPos.z = 0;
		lrWorldBottom.SetPosition (1, vecTempPos);
		lrWorldBottom.startColor = m_aryLineColor [0];
		lrWorldBottom.endColor = m_aryLineColor [0];
	}

	List<CBlock> m_lstRecycledBlocks = new List<CBlock>();
    Dictionary<uint, CBlock> m_dicRecycledBlocks = new Dictionary<uint, CBlock>();

    public CBlock NewBlock( uint uKey )
    {
        CBlock block = null;
		if (m_dicRecycledBlocks.Count > 0) {
			block = m_dicRecycledBlocks.Values.First();
            m_dicRecycledBlocks.Remove(m_dicRecycledBlocks.Keys.First());
            block.SetActive ( true );
            block.ResetAll();
		} else {
			block = GameObject.Instantiate (m_preBlock).GetComponent<CBlock> ();
            block.transform.SetParent(m_goArea0.transform);
            block.SetGuid(s_uBlockGuid++);
        }
		block.SetKey ( uKey );
		AddBlock ( uKey, block );
		return block;
	}

    static uint s_uBlockGuid = 0;
	public void DeleteBlock(CBlock block, bool bDontDelIfBallIn = false)
	{
		RemoveBlock ( block.GetKey() );

        block.SetActive ( false ); // 一定要将其灰掉。Obj的Update()空转也会消耗性能，数量达到一定数量级时一下就显现出来了

        CBlock test_block = null;
        if ( m_dicRecycledBlocks.TryGetValue( block.GetGuid(), out test_block) )
        {
            Debug.LogError( "oh my god !!!!" );
        }
        m_dicRecycledBlocks[block.GetGuid()] = block;
        CBlock parent_block = block.GetParent();
        if (parent_block != null)
        {
            parent_block.RipOutChild(block.GetPosType());
        }

        
        // 递归，将其旗下的子Block也销毁掉
        for (int i = 0; i < 4; i++)
        {
            CBlock child_block = block.m_aryChildBlock[i];

            if (child_block == null)
            {
                continue;
            }
            CBlockDivide.s_Instance.DeleteBlock(child_block);
            block.m_aryChildBlock[i] = null;  
        }
        
    }
	//==========
	Dictionary<uint, CBlock> m_dicBlocks = new Dictionary<uint, CBlock>();
	public void AddBlock( uint uKey, CBlock block )
	{
		m_dicBlocks [uKey] = block;
	}

	public void RemoveBlock(uint uKey)
	{
		m_dicBlocks.Remove ( uKey );
	}

	public CBlock GetBlockByKey( uint uKey )
	{
		CBlock block = null;
		if (!m_dicBlocks.TryGetValue (uKey, out block)) {
			block = null;
		}
		return block;
	}
}
