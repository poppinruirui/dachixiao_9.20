﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPveObj : MonoBehaviour {

	Vector3 m_vScale = new Vector3();
	float m_fRotation = 0f;

	int m_nGuid = 0;
	int m_ConfigId = 0;


	public SpriteRenderer _srMain;

	public 	void OnMouseDown()
	{
		CPveEditor.s_Instance.SelectObj ( this );
	}

	public void SetActive( bool val )
	{
		this.gameObject.SetActive ( val );
	}

	public void SetConfigId( int nId )
	{
		m_ConfigId = nId;
	}

	public int GetConfigId()
	{
		return m_ConfigId;
	}

	public void SetScale( float fScaleX, float fScaleY )
	{
		m_vScale.x = fScaleX;
		m_vScale.y = fScaleY;
		m_vScale.z = 1f;
		this.transform.localScale = m_vScale;
	}

	public void SetScale( Vector3 scale )
	{
		m_vScale = scale;
		this.transform.localScale = m_vScale;
	}

	public Vector3 GetScale( )
	{
		return m_vScale;
	}

	public void SetColor( string szColor )
	{
		Color color = Color.white;
		if ( !ColorUtility.TryParseHtmlString("#" + szColor, out color)) {
			color = Color.white;
		}
		_srMain.color = color;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetPos( Vector3 pos )
	{
		this.transform.position = pos;
	}

	public Vector3 GetPos()
	{
		return this.transform.position;
	}

	public float GetRotation()
	{
		return m_fRotation;
	}

	public void SetRotation( float val )
	{
		m_fRotation = val;
		this.transform.localRotation = Quaternion.identity;
		this.transform.Rotate(0.0f, 0.0f, m_fRotation);
	}


}
