﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CLine : MonoBehaviour {

    public LineRenderer _lrMain;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DrawLine( Vector3 vStartPos, Vector3 vEndPos )
    {
        _lrMain.SetPosition(0, vStartPos);
        _lrMain.SetPosition(1, vEndPos);
    }
}
