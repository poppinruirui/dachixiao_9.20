﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CMsgList : MonoBehaviour {
    
    // prefab
    public GameObject m_preMsgListItem;
    public Image m_preAssistAvatar;

    public float m_fVertSapce;
    public float m_fItemScale;
    public float m_fMoveInterval;
    public float m_fMoveTime;
    public float m_fMoveSpeed;


    List<CMsgListItem> m_lstItems = new List<CMsgListItem>();
    List<CMsgListItem> m_lstRecycledItems = new List<CMsgListItem>();

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public static CMsgList s_Instance = null;
    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        BeginMove();
        Move();

    }

    public CMsgListItem ReuseOneItem()
    {
        CMsgListItem item = null;
        if (m_lstRecycledItems.Count > 0)
        {
            item = m_lstRecycledItems[0];
            m_lstRecycledItems.RemoveAt(0);
            item.gameObject.SetActive(true);
            item.ReInit();
        }
        else
        {
            item = GameObject.Instantiate( m_preMsgListItem ).GetComponent<CMsgListItem>();
        }
        return item;
    }

    public void AddOneItem( string szKillerName, Sprite sprKillerAvatar, List<Sprite> lstAssistAvatar, string szDeadMeatName, Sprite sprDeadMeatAvatar )
    {
        return;

        CMsgListItem item = ReuseOneItem();
        item.SetText1(szKillerName);
        item.SetText2(szDeadMeatName);
        item.SetKillerAvatar(sprKillerAvatar);
        item.SetAssistAvatar(lstAssistAvatar);
        item.SetDeadMeatAvatar(sprDeadMeatAvatar);
        AddOneItem(item);
    }

    public void AddOneItem( string szText1, string szText2)
    {
        CMsgListItem item = ReuseOneItem();
        item.SetText1(szText1);
        item.SetText2(szText2);
        AddOneItem(item);
    }

    public void AddOneItem(CMsgListItem item )
    {
        vecTempPos.x = 0f;
        if (m_lstItems.Count == 0)
        {
            vecTempPos.y = 0f;
        }
        else
        {
            vecTempPos.y = m_lstItems[m_lstItems.Count - 1].transform.localPosition.y - m_fVertSapce;
        }
        vecTempPos.z = 0f;
        m_lstItems.Add(item);
        item.transform.SetParent( this.transform );
        item.transform.localPosition = vecTempPos;

        RectTransform rt = item.gameObject.GetComponent<RectTransform>();
        vecTempScale.x = m_fItemScale;
        vecTempScale.y = m_fItemScale;
        vecTempScale.z = m_fItemScale;
        rt.localScale = vecTempScale;

        
    }

    public void RemoveOneItem(CMsgListItem item)
    {
        item.gameObject.SetActive( false );
        m_lstItems.Remove(item);
        m_lstRecycledItems.Add(item);
    }

    bool m_bMoving = false;
    void BeginMove()
    {
        if (m_lstItems.Count == 0)
        {
            return;
        }

        if (m_bMoving)
        {
            return;
        }

        m_fMoveIntervalCount += Time.deltaTime;
        if ( m_fMoveIntervalCount >= m_fMoveInterval )
        {
            m_bMoving = true;
            m_fMoveIntervalCount = 0f;
            m_fMoveTimeCount = 0f;
        }
    }

    float m_fMoveIntervalCount = 0f;
    float m_fMoveTimeCount = 0f;
    void Move()
    {
        if ( !m_bMoving)
        {
            return;
        }

        for (int  i = m_lstItems.Count - 1; i >= 0;  i-- ) // 如果中途要删除节点，则必须反向遍历
        {
            CMsgListItem item = m_lstItems[i];
            vecTempPos = item.transform.localPosition;
            vecTempPos.y += m_fMoveSpeed * Time.deltaTime;
            item.transform.localPosition = vecTempPos;
            if (item.transform.localPosition.y > 0f )
            {
                item.BeginFade();
            }
            if ( item.FadeCompleted() )
            {
                RemoveOneItem(item)  ;
            }

        }

        m_fMoveTimeCount += Time.deltaTime;
        if (m_fMoveTimeCount >= m_fMoveTime)
        {
            m_bMoving = false;
        }
    }
}
