﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAlpha : MonoBehaviour {

    public CanvasGroup _canvasGroup;
    public float m_fTime = 1f;
    float m_fSpeed = 0;

    int m_nStatus = 0;


	// Use this for initialization
	void Start () {
        m_fSpeed = 1f / m_fTime;
        m_fSpeed *= Time.fixedDeltaTime;
    }
	
	// Update is called once per frame
	void Update () {
		



	}

    private void FixedUpdate()
    {
        if ( m_nStatus == 0 )
        {
            _canvasGroup.alpha -= m_fSpeed;
            if (_canvasGroup.alpha <= 0)
            {
                m_nStatus = 1;
            }
        }
        else
        {
            _canvasGroup.alpha += m_fSpeed;
            if (_canvasGroup.alpha >= 1)
            {
                m_nStatus = 0;
            }
        }
    }

} // end class
