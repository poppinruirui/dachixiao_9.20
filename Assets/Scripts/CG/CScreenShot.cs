﻿using UnityEngine;
using System.Collections;
using System.IO;


public class CScreenShot : MonoBehaviour
{

    public Camera mainCam; //待截图的目标摄像机  
    RenderTexture rt;  //声明一个截图时候用的中间变量   
    Texture2D t2d;
    int num = 0;  //截图计数  

    //public GameObject pl;  //一个调试用的板子  



    void Start()
    {
        t2d = new Texture2D(800, 600, TextureFormat.RGB24, false);
        rt = new RenderTexture(800, 600, 24);
        mainCam.targetTexture = rt;

    }

    void Update()
    {
        //按下空格键来截图  
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //将目标摄像机的图像显示到一个板子上  
            //pl.GetComponent<Renderer>().material.mainTexture = rt;  

            //截图到t2d中  
            RenderTexture.active = rt;
            t2d.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
            t2d.Apply();
            RenderTexture.active = null;

            //将图片保存起来  
            byte[] byt = t2d.EncodeToJPG();
            File.WriteAllBytes(Application.dataPath + "//" + num.ToString() + ".jpg", byt);


            Debug.Log("当前截图序号为：" + num.ToString());
            num++;
        }
    }
}