﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CBuyConfirmUI : MonoBehaviour {
    static Vector3 vecTempScale = new Vector3();

    public static CBuyConfirmUI s_Instance;

    public Button[] m_aryTimeLimitButtons;
    public Button _btnBuy;
    public Text _txtCost;
    public Text _txtCostType;
    public Text _txtGain;
    public Image _imgItem;

    public uint m_nGainNum = 0;
    public int m_nGainType;

    public string m_szProductId = "";    

    // 缩放动画
    public float m_fAnimationScaleSpeed = 1f;
    public float m_fAnimationScaleMin = 0.8f;
    public float m_fAnimationScaleMax = 1.2f;  

    private void Awake()
    {
        s_Instance = this;
    }

    private void FixedUpdate()
    {
        Animation_Scale();
    }

    // Use this for initialization
    void Start () {
        SelectTimeLimitButton(m_aryTimeLimitButtons[0]);
	}
	
    public void OnClickButton_TimeLimitButton0()
    {
        SelectTimeLimitButton(m_aryTimeLimitButtons[0]);
    }

    public void OnClickButton_TimeLimitButton1()
    {
        SelectTimeLimitButton(m_aryTimeLimitButtons[1]);
    }

    public void OnClickButton_TimeLimitButton2()
    {
        SelectTimeLimitButton(m_aryTimeLimitButtons[2]);
    }

	// Update is called once per frame
	void Update () {
		
	}

    public void SelectTimeLimitButton( Button btn )
    {
        for (int i = 0; i < m_aryTimeLimitButtons.Length; i++ )
        {
            Button node_btn = m_aryTimeLimitButtons[i];

            if ( btn == node_btn ) // 选中
            {
                node_btn.gameObject.GetComponentInChildren<Outline8>().enabled = true;
                node_btn.gameObject.GetComponent<Image>().color = ShoppingMallManager.s_Instance.m_colorBtnTimeLimit_Selected;
            }
            else // 未选中
            {
                node_btn.gameObject.GetComponentInChildren<Outline8>().enabled = false;
                node_btn.gameObject.GetComponent<Image>().color = ShoppingMallManager.s_Instance.m_colorBtnTimeLimit_NotSelected;
            }
        }
    }

    public void SetCostType( string szType )
    {
        _txtCostType.text = szType;
    }

    public void SetCostNum( string szNum )
    {
        _txtCost.text = "x" + szNum;
    }

    public void SetGain( string szGain )
    {
        _txtGain.text = szGain;
    }

    public void SetProductId( string szProductId )
    {
        m_szProductId = szProductId;
    }

    public void SetProductAvatar( Sprite spr )
    {
        _imgItem.sprite = spr;
    }

    public string GetProductId()
    {
        return m_szProductId;
    }

    void Animation_Scale()
    {
        if (
            (m_fAnimationScaleSpeed > 0 && _btnBuy.transform.localScale.x >= m_fAnimationScaleMax) ||
            (m_fAnimationScaleSpeed < 0 && _btnBuy.transform.localScale.x <= m_fAnimationScaleMin)
           )
        {
            m_fAnimationScaleSpeed = -m_fAnimationScaleSpeed;
        }

        vecTempScale = _btnBuy.transform.localScale;
        float fChangeAmount = m_fAnimationScaleSpeed * Time.fixedDeltaTime;
        vecTempScale.x += fChangeAmount;
        vecTempScale.y += fChangeAmount;
        _btnBuy.transform.localScale = vecTempScale;
    }

}
