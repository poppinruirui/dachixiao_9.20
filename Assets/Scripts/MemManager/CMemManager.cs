﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMemManager : MonoBehaviour {

    public static CMemManager s_Instance = null;

    private void Awake()
    {
        //// DontDestroyOnLoad很坑，每当回到这个场景，该对象会再创建一次，导致这个对象可能变得无限多个。所以要处理一下
        if (s_Instance != null)
        {
            GameObject.Destroy(this.gameObject);


            return;
        }
        s_Instance = this;
        GameObject.DontDestroyOnLoad(this);
        //// 

    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
 
    public void LoadPlayerDataFromLocal( string szCosmosAccount )
    {
        
    }

}
