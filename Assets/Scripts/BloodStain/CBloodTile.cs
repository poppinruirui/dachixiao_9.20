﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBloodTile : MonoBehaviour {

    public SpriteRenderer m_sprMain;
    public GameObject m_goMainContainer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetActive(bool bActive)
    {
        m_goMainContainer.SetActive(bActive);
    }

    public void SetColor( Color color )
    {
        m_sprMain.color = color;
    }
}
