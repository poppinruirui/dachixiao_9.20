﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiggerGuangZi : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public Ball _ball;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        Ball ballOpponent = collision.transform.parent.gameObject.GetComponent<Ball>();
        if (ballOpponent == null)
        {
            Debug.LogError( "bug" );
            return;
        }

        if (!ballOpponent._Player.IsSneaking())
        {
            return;
        }

        //if (ballOpponent._Player.GetBallAppeared(ballOpponent.GetIndex()))
        if (ballOpponent._Player.GetBallAppearedByGuid(ballOpponent.GetGuid()) )
        {
            return;
        }
      
        // ballOpponent._Player.LetItAppear();
        ballOpponent._Player.SetBallAppearedByGuid(ballOpponent.GetGuid() );//SetBallAppeared( ballOpponent.GetIndex(), true );
        ballOpponent.SetAlpha(0.3f);

        vecTempPos = ballOpponent.GetPos();
   
       
        vecTempPos.y+= ballOpponent.GetSize();

        //// “弹开”跳字
        CTiaoZi tiaozi = CTiaoZiManager.s_Instance.NewTiaoZi();
        tiaozi.SetPos(vecTempPos);
        tiaozi.SetText(0, "现形");
        float fTiaoZiScale = CTiaoZiManager.s_Instance.m_fTiaoZiCamThreshold_TanKai * CCameraManager.s_Instance.GetCurCameraSize();
        tiaozi.SetScale(fTiaoZiScale);
        tiaozi.SetTextAlign(0, TextAnchor.MiddleCenter);
        tiaozi.SetTextColor(0, Color.white);
        tiaozi.SetTitleSprVisible(false);
        tiaozi.SetParams_Type4(CTiaoZiManager.s_Instance.m_fShengJi_ChiXuTime);


            GameObject effectAppear = CEffectManager.s_Instance.NewEffect_Appear();
            
        effectAppear.transform.SetParent(ballOpponent._containerEffect.transform);

            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            effectAppear.transform.localScale = vecTempScale;


            vecTempPos.x = 0f;
            vecTempPos.y = 0f;
            vecTempPos.z = 0f;
            effectAppear.transform.localPosition = vecTempPos;

            ParticleSystem ps = effectAppear.GetComponent<ParticleSystem>();
            ps.startSize = ballOpponent.GetSize();
       




    }


    private void OnTriggerExit2D(Collider2D collision)
    {

    }


} // end class
