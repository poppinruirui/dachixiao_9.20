﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAdaptiveManager : MonoBehaviour {

    public static CAdaptiveManager s_Instance = null;

    public bool m_bCheckDEvice = true;

    public CPaoMaDeng _paomadeng;
    public CPaoMaDeng[] m_aryPaoMaDeng;

    /// <summary>
    /// / 适配数值
    /// </summary>
    public float[] m_aryLongTimeShowPanelScale;
    public Vector2[] m_aryLongTimeShowPanelPos;

    public float[] m_arySelectHeroScale;
    public Vector2[] m_arySelectHeroPos;

    public float[] m_aryHeroInfoScale;
    public Vector2[] m_aryHeroInfoPos;

    public Vector2[] m_arySkillPanelPrevButtonPos;

    /// end 适配数值 




    public enum eDeviceTYpe
    {
        iPhone_X,
        Others,
    };
    public eDeviceTYpe m_eDeviceType;

    public Vector2[] m_Resolution;

    // Use this for initialization
    void Start () {

        string szDeviceType = SystemInfo.deviceModel;
        if (szDeviceType == "iPhone10,3" || szDeviceType == "iPhone10,6")
        {
            szDeviceType = "iPhone X";
        }
        else if (szDeviceType == "iPhone9,2" || szDeviceType == "iPhone9,4")
        {
            szDeviceType = "iPhone 7 Plus";
        }
        GetPaoMaDeng().SetContent( 0, "当前设备型号：" + szDeviceType);
    }
	
    CPaoMaDeng GetPaoMaDeng()
    {
        return m_aryPaoMaDeng[(int)GetCurDeviceType()];
    }

	// Update is called once per frame
	void Update () {
		
	}

    private void Awake()
    {
        CheckDeviceType();

        //// DontDestroyOnLoad很坑，每当回到这个场景，该对象会再创建一次，导致这个对象可能变得无限多个。所以要处理一下
        if (s_Instance != null)
        {
            GameObject.Destroy(this.gameObject);


            return;
        }
        s_Instance = this;
        GameObject.DontDestroyOnLoad(this);
        //// 

    }

    public void CheckDeviceType()
    {
        if (!m_bCheckDEvice )
        {
            return;
        }

        if (SystemInfo.deviceModel == "iPhone10,3" || SystemInfo.deviceModel == "iPhone10,6")
        {
            m_eDeviceType = eDeviceTYpe.iPhone_X;
        }
        else
        {
            m_eDeviceType = eDeviceTYpe.Others;
        }
    }

    public eDeviceTYpe GetCurDeviceType()
    {
        return m_eDeviceType;
    }

    public Vector2 GetResolution()
    {
        return m_Resolution[(int)GetCurDeviceType()];
    }
}
