﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameManager : MonoBehaviour {

    public static NameManager s_Instance = null;

     string[] m_aryNames =
    {
       " 你隔壁家的王叔叔",
"香蕉你个麻辣",
"冒牌小冬瓜",
"两包辣条约吗",
"浪荡街痞",
"多余似我",
"风吹随他去",
"a1過于自大",
       "颓丧的少年@",
"[毁本人]",
    "ぅ化骨绵掌",
"呼吸☆压制",
"我以爲我可以。",
"思念、其实漠然ㄋ",
"花、腐败的香。",
"这心不属于我",
"装逼遭雷劈从头劈到逼",
"难免心死祝我活该",
"荒度余生o",
"你看我有七个字",
"没对象ペ省流量",
"跟著姐姐有糖吃",
"牵羊逛街的狼",
"蛆在厕所等你",
"穷人说话牙没力",
"你长得真玩命！",
"村里一枝花",
"辢条狂魔",
"小熊穿了比基尼",

    };


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public string GetRandomName()
    {
        int nIndex = UnityEngine.Random.Range(0, 7);
        if ( nIndex < 0 || nIndex >= m_aryNames.Length)
        {
            nIndex = 0;
        }
        return m_aryNames[nIndex];

    }


} // end class
