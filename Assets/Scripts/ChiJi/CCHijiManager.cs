﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CCHijiManager : MonoBehaviour {

    public static CCHijiManager s_Instance = null;

    static Color colorTemp = new Color();

    public int m_nGameStatus = 0; // 0 - waiting  1 - fighting  2 - End (Win or Lose)
   

    public int m_nPlayerNumToStart = 3;

    public GameObject _panelWaiting; //
    public GameObject _panelWin;
    public GameObject _panelLose;

    public Text _txtPlayerNum;
    public Text _txtCurLivePlayerNum;
    public Text _txtCurLivePlayerNum_Shadow;

    public Text _txtRank;

    bool m_bWin = false;
    bool m_bLose = false;

    float m_fWinTimeCount = 0;
    float m_fLoseTimeCount = 0;

    /// <summary>
    /// / “死亡地带”相关
    /// </summary>
    public SpriteRenderer[] _goDeadMask;

    public float m_fDeadAreaMaskMaxAlpha = 1f;
    public float m_fDeadAreaMaskMinAlpha = 1f;
    public int m_nDeadAreaMaskStatus = 0; // 0 - fade out   1 - fade in
    public float m_fDeadAreaMaskFadeTime = 1f;
    float m_fDeadAreaMaskFadeSpeed = 1f;
    float m_fDeadAreaMaskCurAlpha = 0;

    bool m_bCanExit = false;

    public bool m_bDoNotAutoChangeScene = false;

    private void Awake()
    {
        s_Instance = this;
    }


    // Use this for initialization
    void Start () {

        m_fDeadAreaMaskFadeSpeed = (m_fDeadAreaMaskMaxAlpha - m_fDeadAreaMaskMinAlpha) / m_fDeadAreaMaskFadeTime;

        m_fDeadAreaMaskCurAlpha = m_fDeadAreaMaskMaxAlpha;
    }

    private void FixedUpdate()
    {
        DeadAreaMaskLoop();
    }

    // Update is called once per frame
    void Update () {

        if ( !Main.s_Instance.m_bChiJi )
        {
            return;
        }

        MainLoop();
        MainLoop_1000MiniSec();


    }

    void MainLoop()
    {
        Waiting_Loop();
        Fighting_Loop();

        WinLoop();
        LoseLoop();
    }

    float m_fTimeElapse1000MiniSec = 0;
    void MainLoop_1000MiniSec()
    {
        if (PhotonNetwork.room == null)
        {
            return;
        }

        m_fTimeElapse1000MiniSec += Time.deltaTime;
        if (m_fTimeElapse1000MiniSec < 1f)
        {
            return;
        }
        m_fTimeElapse1000MiniSec = 0;


        _txtCurLivePlayerNum.text = PhotonNetwork.room.playerCount.ToString() ;
        _txtCurLivePlayerNum_Shadow.text = _txtCurLivePlayerNum.text;
    }

    void Waiting_Loop()
    {
        if (m_nGameStatus != 0)
        {
            return;
        }

        if (PhotonNetwork.room == null)
        {
            return;
        }

        if ( PhotonNetwork.room.playerCount >= m_nPlayerNumToStart )
        {
            _panelWaiting.SetActive(false);
            m_nGameStatus = 1;
        }
        else
        {
            _txtPlayerNum.text = "当前已登入：" + PhotonNetwork.room.playerCount + " 人";// + "/" + m_nPlayerNumToStart;
          // _panelWaiting.SetActive( true );
        }


    }

    void Fighting_Loop()
    {
        if (m_nGameStatus != 1)
        {
            return;
        }

        if (PhotonNetwork.room == null)
        {
            return;
        }

        if ( PhotonNetwork.room.playerCount == 1 )
        {
            YouWin();
        }

    }

    public void YouWin()
    {
        m_nGameStatus = 2;
        PlayerPrefs.SetString("GameId", "");
        Main.s_Instance.m_MainPlayer.UploadGameData(1, true);
        m_fWinTimeCount = 0;
        m_bWin = true;

        _panelWin.SetActive( true );

        Debug.Log( "you win!!!!!!!!!!!!!!!" );
    }

    public void YouLose()
    {
        m_nGameStatus = 2;
        PlayerPrefs.SetString("GameId", "");
        m_bLose = true;
        m_fLoseTimeCount = 0;
        _panelLose.SetActive(true);
        Main.s_Instance.groupHashtable();

        _txtRank.text = PhotonNetwork.room.playerCount.ToString();

        m_bDoNotAutoChangeScene = true;
    }

    void WinLoop()
    {
        if ( !m_bWin)
        {
            return;
        }

        m_fWinTimeCount += Time.deltaTime;
        if (m_fWinTimeCount >= 1f)
        {
            m_bCanExit = true;
        }
        if (m_fWinTimeCount >= 30)
        {
            End();
        }

       

        if (m_bCanExit)
        {
            if (Input.GetMouseButtonDown(0))
            {
                End();
            }
        }
    }

    void End()
    {
       

        PlayerPrefs.SetString("GameId", "");
        AccountManager.s_bGaming = false;
        Main.s_Instance.groupHashtable();
        SceneManager.LoadScene( "SelectCaster" );
    }


    void LoseLoop()
    {

        if ( !m_bLose )
        {
            return;
        }

        m_fLoseTimeCount += Time.deltaTime;
        if (m_fLoseTimeCount >= 1f)
        {
            m_bCanExit = true;
        }

        if (m_fLoseTimeCount >= 30f)
        {
            End();
        }

        if (m_bCanExit)
        {
            if (Input.GetMouseButtonDown(0))
            {
                End();
            }
        }

    }

    void DeadAreaMaskLoop()
    {
        if ( m_nDeadAreaMaskStatus == 0 )
        {
            m_fDeadAreaMaskCurAlpha -= m_fDeadAreaMaskFadeSpeed * Time.fixedDeltaTime;
            if (m_fDeadAreaMaskCurAlpha <= m_fDeadAreaMaskMinAlpha)
            {
                m_nDeadAreaMaskStatus = 1;
            }
       

        }
        else if (m_nDeadAreaMaskStatus == 1)
        {
            m_fDeadAreaMaskCurAlpha += m_fDeadAreaMaskFadeSpeed * Time.fixedDeltaTime;
            if (m_fDeadAreaMaskCurAlpha >= m_fDeadAreaMaskMaxAlpha)
            {
                m_nDeadAreaMaskStatus = 0;
            }
        }


        for (int i = 0; i < _goDeadMask.Length; i++)
        {
            SpriteRenderer sr = _goDeadMask[i];
            colorTemp = sr.color;
            colorTemp.a = m_fDeadAreaMaskCurAlpha;
            sr.color = colorTemp;
        }
    }


}
