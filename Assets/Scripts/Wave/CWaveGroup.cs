﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CWaveGroup : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    List<CWave> m_lst = new List<CWave>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetScale(float fScale)
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetPos(Vector3 pos)
    {
        this.transform.localPosition = pos;
    }

    public void AddWave( CWave wave )
    {
        m_lst.Add( wave );
        wave.transform.SetParent(this.transform);
    }

    public void SetAlpha( float fAlpha )
    {
        for ( int i = 0; i < m_lst.Count; i++ )
        {
            m_lst[i].SetAlpha(fAlpha);
        }
    }
}
